export const getProvider = id => ({
  type: 'GET_PROVIDER',
  payload: {
    request: {
      url: `/providers/${id}`,
      method: 'get',
    },
  },
});

export const addProvider = data => ({
  type: 'POST_PROVIDER',
  payload: {
    request: {
      url: '/providers',
      method: 'post',
      data,
    },
  },
});

export const updateProvider = (id, data) => ({
  type: 'UPDATE_PROVIDER',
  payload: {
    request: {
      url: `/providers/${id}`,
      method: 'put',
      data,
    },
  },
});

export const deleteProviders = ids => ({
  type: 'DELETE_PROVIDERS',
  payload: {
    request: {
      url: `/providers/${ids.join()}`,
      method: 'delete',
    },
  },
});

export const getProviders = () => ({
  type: 'GET_PROVIDERS',
  payload: {
    request: {
      url: '/providers',
      method: 'get',
    },
  },
});

export const getProviderProduct = (provider_id, product_id) => ({
  type: 'GET_PROVIDER_PRODUCT',
  payload: {
    request: {
      url: `/providers/${provider_id}/products/${product_id}`,
      method: 'get',
    },
  },
});

export const addProviderProduct = (provider_id, data) => ({
  type: 'POST_PROVIDER_PRODUCT',
  payload: {
    request: {
      url: `/providers/${provider_id}/products`,
      method: 'post',
      data,
    },
  },
});

export const updateProviderProduct = (provider_id, product_id, data) => ({
  type: 'UPDATE_PROVIDER_PRODUCT',
  payload: {
    request: {
      url: `/providers/${provider_id}/products/${product_id}`,
      method: 'put',
      data,
    },
  },
});

export const deleteProviderProducts = (provider_id, product_ids) => ({
  type: 'DELETE_PROVIDER_PRODUCTS',
  payload: {
    request: {
      url: `/providers/${provider_id}/products/${product_ids.join()}`,
      method: 'delete',
    },
  },
});

export const getProviderProducts = provider_id => ({
  type: 'GET_PROVIDER_PRODUCTS',
  payload: {
    provider_id,
    request: {
      url: `/providers/${provider_id}/products`,
      method: 'get',
    },
  },
});

