export const getProductTypes = () => ({
  type: 'GET_PRODUCT_TYPES',
  payload: {
    request: {
      url: '/productTypes',
      method: 'get',
    },
  },
});
