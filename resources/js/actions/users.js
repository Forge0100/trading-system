export const getUser = id => ({
  type: 'GET_USER',
  payload: {
    request: {
      url: `/users/${id}`,
      method: 'get',
    },
  },
});

export const addUser = data => ({
  type: 'POST_USER',
  payload: {
    request: {
      url: '/users',
      method: 'post',
      data,
    },
  },
});

export const updateUser = (id, data) => ({
  type: 'UPDATE_USER',
  payload: {
    request: {
      url: `/users/${id}`,
      method: 'put',
      data,
    },
  },
});

export const deleteUsers = ids => ({
  type: 'DELETE_USERS',
  payload: {
    request: {
      url: `/users/${ids.join()}`,
      method: 'delete',
    },
  },
});

export const getUsers = () => ({
  type: 'GET_USERS',
  payload: {
    request: {
      url: '/users',
      method: 'get',
    },
  },
});
