export const getProduct = id => ({
  type: 'GET_PRODUCT',
  payload: {
    request: {
      url: `/products/${id}`,
      method: 'get',
    },
  },
});

export const addProduct = data => ({
  type: 'POST_PRODUCT',
  payload: {
    request: {
      url: '/products',
      method: 'post',
      data,
    },
  },
});

export const updateProduct = (id, data) => ({
  type: 'UPDATE_PRODUCT',
  payload: {
    request: {
      url: `/products/${id}`,
      method: 'put',
      data,
    },
  },
});

export const deleteProducts = ids => ({
  type: 'DELETE_PRODUCTS',
  payload: {
    request: {
      url: `/products/${ids.join()}`,
      method: 'delete',
    },
  },
});

export const getProducts = () => ({
  type: 'GET_PRODUCTS',
  payload: {
    request: {
      url: '/products',
      method: 'get',
    },
  },
});
