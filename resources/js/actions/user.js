export const login = data => ({
  type: 'GET_AUTH',
  payload: {
    request: {
      url: '/login',
      method: 'post',
      data,
    },
  },
});

export const register = data => ({
  type: 'SET_AUTH',
  payload: {
    request: {
      url: '/register',
      method: 'post',
      data,
    },
  },
});
