export const getStorage = id => ({
  type: 'GET_STORAGE',
  payload: {
    request: {
      url: `/storages/${id}`,
      method: 'get',
    },
  },
});

export const addStorage = data => ({
  type: 'POST_STORAGE',
  payload: {
    request: {
      url: '/storages',
      method: 'post',
      data,
    },
  },
});

export const updateStorage = (id, data) => ({
  type: 'UPDATE_STORAGE',
  payload: {
    request: {
      url: `/storages/${id}`,
      method: 'put',
      data,
    },
  },
});

export const deleteStorages = ids => ({
  type: 'DELETE_STORAGES',
  payload: {
    request: {
      url: `/storages/${ids.join()}`,
      method: 'delete',
    },
  },
});

export const getStorages = () => ({
  type: 'GET_STORAGES',
  payload: {
    request: {
      url: '/storages',
      method: 'get',
    },
  },
});
