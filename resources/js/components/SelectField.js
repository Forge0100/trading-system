import React from 'react';
import Select from '@material-ui/core/Select';

export default ({
  label,
  input,
  meta: { touched, invalid },
  ...custom
}) => (
  <Select
    label={label}
    placeholder={label}
    error={touched && invalid}
    {...input}
    {...custom}
  />
);
