import React from 'react';
import TextField from '@material-ui/core/TextField';
import { ErrorMessages } from 'react-nebo15-validate';

export default ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    placeholder={label}
    error={touched && invalid}
    helperText={touched && error && <ErrorMessages error={error} />}
    {...input}
    {...custom}
  />
);
