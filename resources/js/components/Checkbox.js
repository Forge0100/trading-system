import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export default ({ input, label }) => (
  <div>
    <FormControlLabel
      control={(
        <Checkbox
          checked={!!input.value}
          onChange={input.onChange}
        />
      )}
      label={label}
    />
  </div>
);
