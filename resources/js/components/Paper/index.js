import React from 'react';
import MuiPaper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import styles from './styles';

const Paper = ({ classes, ...rest }) => (
  <MuiPaper className={classes.paper} {...rest} />
);

export default withStyles(styles)(Paper);
