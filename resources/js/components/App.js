import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import ReactDOM from 'react-dom';
import { Switch } from 'react-router';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import DashboardLayout from '~/containers/layouts/Dashboard';
import Index from '~/containers/pages/Index';
import Login from '~/containers/pages/Login';
import Products from '~/containers/pages/Products';
import Product from '~/containers/pages/Product';
import ProductNew from '~/containers/pages/ProductNew';
import Users from '~/containers/pages/Users';
import User from '~/containers/pages/User';
import UserNew from '~/containers/pages/UserNew';
import Providers from '~/containers/pages/Providers';
import Provider from '~/containers/pages/Provider';
import ProviderNew from '~/containers/pages/ProviderNew';
import ProviderProducts from '~/containers/pages/ProviderProducts';
import ProviderProduct from '~/containers/pages/ProviderProduct';
import ProviderProductNew from '~/containers/pages/ProviderProductNew';
import Storages from '~/containers/pages/Storages';
import Storage from '~/containers/pages/Storage';
import StorageNew from '~/containers/pages/StorageNew';
import store from '~/store';

const App = () => (
  <Router>
    <Switch>
      <Route path="/login" exact component={Login} />
      <DashboardLayout>
        <Route path="/" exact component={Index} />
        <Route path="/products" exact component={Products} />
        <Route path="/products/new" exact component={ProductNew} />
        <Route path="/products/:id(\d+)" exact component={Product} />
        <Route path="/users" exact component={Users} />
        <Route path="/users/new" exact component={UserNew} />
        <Route path="/users/:id(\d+)" exact component={User} />
        <Route path="/providers" exact component={Providers} />
        <Route path="/providers/new" exact component={ProviderNew} />
        <Route path="/providers/:id(\d+)" exact component={Provider} />
        <Route path="/providers/:provider_id(\d+)/products" exact component={ProviderProducts} />
        <Route path="/providers/:provider_id(\d+)/products/new" exact component={ProviderProductNew} />
        <Route path="/providers/:provider_id(\d+)/products/:product_id(\d+)" exact component={ProviderProduct} />
        <Route path="/storages" exact component={Storages} />
        <Route path="/storages/new" exact component={StorageNew} />
        <Route path="/storages/:id(\d+)" exact component={Storage} />
      </DashboardLayout>
    </Switch>
  </Router>
);

if (document.getElementById('app')) {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <App />
    </ReduxProvider>,
    document.getElementById('app'),
  );
}
