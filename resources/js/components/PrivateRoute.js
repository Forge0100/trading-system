import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import store from '~/store';

export default ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (store.getState().token
      ? <Component {...props} />
      : <Redirect to={{ pathname: '/login' }} />)}
  />
);
