import React from 'react';
import { compact } from 'lodash';
import MUIDataTable from 'mui-datatables';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AddIcon from '@material-ui/icons/Add';

const Table = ({
  columns,
  data,
  options: {
    onAddButtonClick,
    onEditButtonClick,
    onRowsDeleteClick,
    ...otherOptions
  },
  ...rest
}) => (
  <MUIDataTable
    columns={compact([
      ...columns,
      !columns.find(column => column.name === 'id') && {
        name: 'id',
        label: 'Actions',
        options: {
          filter: false,
          sort: false,
          empty: true,
          customBodyRender: value => (
            <Button
              color="primary"
              onClick={() => onEditButtonClick(value)}
            >
              Edit
            </Button>
          ),
        },
      },
    ])}
    options={{
      customToolbar: () => (
        <Tooltip title="Add">
          <IconButton onClick={onAddButtonClick}>
            <AddIcon />
          </IconButton>
        </Tooltip>
      ),
      onRowsDelete: (rowsDeleted) => {
        const ids = rowsDeleted.data.map(({ dataIndex }) => data[dataIndex].id);

        onRowsDeleteClick(ids);
      },
      ...otherOptions,
    }}
    data={data}
    {...rest}
  />
);

export default Table;
