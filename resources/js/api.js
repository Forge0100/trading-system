import axios from 'axios';

export const api = axios.create({
  baseURL: process.env.API_URL,
});

export const axiosMiddlewareOptions = {
  returnRejectedPromiseOnError: true,
  interceptors: {
    request: [
      ({ getState }, config) => {
        const { token } = getState();

        if (token) {
          config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
      },
    ],
    response: [
      {
        success: (_, response) => response,
        error: (_, error) => Promise.reject(error),
      },
    ],
  },
};
