import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { reduxFormValidate } from 'react-nebo15-validate';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import withStyles from '@material-ui/core/styles/withStyles';
import TextField from '~/components/TextField';
import Checkbox from '~/components/Checkbox';
import styles from './styles';

const Login = ({
  classes,
  handleSubmit,
  pristine,
  submitting,
}) => (
  <form className={classes.form} onSubmit={handleSubmit}>
    <FormControl margin="normal" required fullWidth>
      <Field
        name="email"
        component={TextField}
        type="text"
        placeholder="Email Address"
        autoComplete="email"
        autoFocus
        fullWidth
      />
    </FormControl>
    <FormControl margin="normal" required fullWidth>
      <Field
        name="password"
        component={TextField}
        type="password"
        placeholder="Password"
        autoComplete="current-password"
        fullWidth
      />
    </FormControl>
    <Field
      name="remember"
      component={Checkbox}
      label="Remember me"
    />
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="primary"
      className={classes.submit}
      disabled={pristine || submitting}
    >
      Sign in
    </Button>
  </form>
);

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles),
  reduxForm({
    form: 'login',
    validate: reduxFormValidate({
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
      },
    }),
  }),
)(Login);
