import React from 'react';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { reduxFormValidate } from 'react-nebo15-validate';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import TextField from '~/components/TextField';
import SelectField from '~/components/SelectField';

const Product = ({
  productTypeOptions = [],
  handleSubmit,
  invalid,
  submitting,
}) => (
  <form onSubmit={handleSubmit}>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={6}>
        <Field
          name="model"
          component={TextField}
          type="text"
          placeholder="Model"
          autoComplete="model"
          autoFocus
          fullWidth
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field
          name="producer"
          component={TextField}
          type="text"
          placeholder="Producer"
          autoComplete="producer"
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Field
          name="price"
          component={TextField}
          type="number"
          placeholder="Price"
          autoComplete="price"
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Field
          name="warranty"
          component={TextField}
          type="number"
          placeholder="Warranty"
          autoComplete="warranty"
          fullWidth
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field
          name="description"
          component={TextField}
          type="text"
          placeholder="Description"
          autoComplete="description"
          rowsMax="4"
          fullWidth
          multiline
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field
          name="product_type_id"
          component={SelectField}
          type="text"
          placeholder="Description"
          autoComplete="product_type_id"
          fullWidth
        >
          {productTypeOptions.map(option => (
            <MenuItem key={option.id} value={option.id}>
              {option.name}
            </MenuItem>
          ))}
        </Field>
      </Grid>
    </Grid>
    <Grid container justify="flex-end" style={{ marginTop: 16 }}>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={invalid || submitting}
      >
        Save
      </Button>
    </Grid>
  </form>
);

export default compose(
  reduxForm({
    form: 'product',
    enableReinitialize: true,
    validate: reduxFormValidate({
      model: {
        required: true,
        minLength: 4,
      },
      producer: {
        required: true,
        minLength: 4,
      },
      price: {
        required: true,
      },
      warranty: {
        required: true,
      },
      description: {
        required: true,
        minLength: 30,
      },
      product_type_id: {
        required: true,
      },
    }),
  }),
)(Product);
