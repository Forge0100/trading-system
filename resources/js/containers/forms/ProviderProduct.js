import React from 'react';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { reduxFormValidate } from 'react-nebo15-validate';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import TextField from '~/components/TextField';
import SelectField from '~/components/SelectField';

const ProviderProduct = ({
  products,
  handleSubmit,
  invalid,
  submitting,
}) => (
  <form onSubmit={handleSubmit}>
    <Grid container spacing={24}>
      <Grid item xs={12} sm={6}>
        <Field
          name="product_id"
          component={SelectField}
          type="text"
          autoComplete="product_id"
          displayEmpty
          fullWidth
        >
          <MenuItem value="" disabled>Product</MenuItem>
          {products.map(option => (
            <MenuItem key={option.id} value={option.id}>
              {option.name}
            </MenuItem>
          ))}
        </Field>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Field
          name="price"
          component={TextField}
          type="text"
          placeholder="Price"
          autoComplete="price"
          fullWidth
        />
      </Grid>
    </Grid>
    <Grid container justify="flex-end" style={{ marginTop: 16 }}>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={invalid || submitting}
      >
        Save
      </Button>
    </Grid>
  </form>
);

export default compose(
  reduxForm({
    form: 'providerProduct',
    enableReinitialize: true,
    validate: reduxFormValidate({
      product_id: {
        required: true,
      },
      price: {
        required: true,
      },
    }),
  }),
)(ProviderProduct);
