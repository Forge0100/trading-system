import React from 'react';
import { compose } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { reduxFormValidate } from 'react-nebo15-validate';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '~/components/TextField';

const Storage = ({
  handleSubmit,
  invalid,
  submitting,
}) => (
  <form onSubmit={handleSubmit}>
    <Grid container spacing={24}>
      <Grid item xs={12}>
        <Field
          name="name"
          component={TextField}
          type="text"
          placeholder="Name"
          autoComplete="name"
          autoFocus
          fullWidth
        />
      </Grid>
      <Grid item xs={12}>
        <Field
          name="address"
          component={TextField}
          type="text"
          placeholder="Address"
          autoComplete="address"
          fullWidth
        />
      </Grid>
    </Grid>
    <Grid container justify="flex-end" style={{ marginTop: 16 }}>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        disabled={invalid || submitting}
      >
        Save
      </Button>
    </Grid>
  </form>
);

export default compose(
  reduxForm({
    form: 'storage',
    enableReinitialize: true,
    validate: reduxFormValidate({
      name: {
        required: true,
      },
    }),
  }),
)(Storage);
