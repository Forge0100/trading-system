import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProductForm from '~/containers/forms/Product';
import { addProduct } from '~/actions/products';
import { getProductTypes } from '~/actions/productTypes';

class ProductNew extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { getProductTypes } = this.props;

    getProductTypes();
  }

  handleSubmit(values) {
    const {
      history,
      addProduct,
    } = this.props;

    addProduct(values).then((response) => {
      history.replace(`/products/${response.payload.data.data.id}`);
    });
  }

  render() {
    const { handleSubmit } = this;
    const {
      token,
      productTypes,
    } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Add product</Typography>
        <ProductForm
          productTypeOptions={productTypes}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  productTypes: state.productTypes,
}), dispatch => bindActionCreators({
  addProduct,
  getProductTypes,
}, dispatch))(ProductNew);
