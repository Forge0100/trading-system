import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Table from '~/components/Table';
import { getStorages, deleteStorages } from '~/actions/storages';

class Storages extends Component {
  constructor(props) {
    super(props);

    this.handleAddButtonClick = this.handleAddButtonClick.bind(this);
    this.handleEditButtonClick = this.handleEditButtonClick.bind(this);
    this.handleRowsDeleteClick = this.handleRowsDeleteClick.bind(this);
  }

  componentDidMount() {
    const { getStorages } = this.props;

    getStorages();
  }

  handleAddButtonClick() {
    const { history } = this.props;

    history.push('/storages/new');
  }

  handleEditButtonClick(index) {
    const { history } = this.props;

    history.push(`/storages/${index}`);
  }

  handleRowsDeleteClick(ids) {
    const { deleteStorages } = this.props;

    deleteStorages(ids);
  }

  render() {
    const {
      handleAddButtonClick,
      handleEditButtonClick,
      handleRowsDeleteClick,
    } = this;
    const { token, storages } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Table
        title="Storages"
        columns={[
          {
            name: 'name',
            label: 'Name',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'address',
            label: 'Address',
            options: {
              filter: true,
              sort: true,
            },
          },
        ]}
        data={storages}
        options={{
          onAddButtonClick: handleAddButtonClick,
          onEditButtonClick: handleEditButtonClick,
          onRowsDeleteClick: handleRowsDeleteClick,
        }}
      />
    );
  }
}

export default connect(state => ({
  token: state.token,
  storages: state.storages,
}), dispatch => bindActionCreators({
  getStorages,
  deleteStorages,
}, dispatch))(Storages);
