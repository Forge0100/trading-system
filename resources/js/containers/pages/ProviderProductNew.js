import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProviderProductForm from '~/containers/forms/ProviderProduct';
import {
  getProviders,
  addProviderProduct,
} from '~/actions/providers';
import { getProducts } from '~/actions/products';

class ProviderProductNew extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      getProviders,
      getProducts,
    } = this.props;

    getProviders();
    getProducts();
  }

  static getDerivedStateFromProps(nextProps) {
    const {
      products: rawProducts,
    } = nextProps;

    const products = rawProducts.map(product => ({
      ...product,
      name: product.model,
    }));

    return {
      products,
    };
  }

  handleSubmit(values) {
    const {
      match: {
        params: { provider_id },
      },
      history,
      addProviderProduct,
    } = this.props;

    addProviderProduct(provider_id, values).then(() => {
      history.push(`/providers/${provider_id}/products`);
    });
  }

  render() {
    const { handleSubmit } = this;
    const {
      token,
      providers,
    } = this.props;
    const {
      products,
    } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Add provider product</Typography>
        <ProviderProductForm
          providers={providers}
          products={products}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  providers: state.providers,
  products: state.products,
}), dispatch => bindActionCreators({
  getProviders,
  addProviderProduct,
  getProducts,
}, dispatch))(ProviderProductNew);
