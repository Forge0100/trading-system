import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Table from '~/components/Table';
import { getUsers, deleteUsers } from '~/actions/users';

class Users extends Component {
  constructor(props) {
    super(props);

    this.handleAddButtonClick = this.handleAddButtonClick.bind(this);
    this.handleEditButtonClick = this.handleEditButtonClick.bind(this);
    this.handleRowsDeleteClick = this.handleRowsDeleteClick.bind(this);
  }

  componentDidMount() {
    const { getUsers } = this.props;

    getUsers();
  }

  handleAddButtonClick() {
    const { history } = this.props;

    history.push('/users/new');
  }

  handleEditButtonClick(index) {
    const { history } = this.props;

    history.push(`/users/${index}`);
  }

  handleRowsDeleteClick(ids) {
    const { deleteUsers } = this.props;

    deleteUsers(ids);
  }

  render() {
    const {
      handleAddButtonClick,
      handleEditButtonClick,
      handleRowsDeleteClick,
    } = this;
    const { token, users } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Table
        title="Users"
        columns={[
          {
            name: 'name',
            label: 'Name',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'surname',
            label: 'Surname',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'patronymic',
            label: 'Patronymic',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'date_of_birth',
            label: 'Date of birth',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'phone',
            label: 'Phone',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'email',
            label: 'Email',
            options: {
              filter: true,
              sort: true,
            },
          },
        ]}
        data={users}
        options={{
          onAddButtonClick: handleAddButtonClick,
          onEditButtonClick: handleEditButtonClick,
          onRowsDeleteClick: handleRowsDeleteClick,
        }}
      />
    );
  }
}

export default connect(state => ({
  token: state.token,
  users: state.users,
}), dispatch => bindActionCreators({
  getUsers,
  deleteUsers,
}, dispatch))(Users);
