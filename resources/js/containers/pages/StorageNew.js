import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import StorageForm from '~/containers/forms/Storage';
import {
  addStorage,
} from '~/actions/storages';

class StorageNew extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    const {
      history,
      addStorage,
    } = this.props;

    addStorage(values).then((response) => {
      history.replace(`/storages/${response.payload.data.data.id}`);
    });
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Add storage</Typography>
        <StorageForm
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
}), dispatch => bindActionCreators({
  addStorage,
}, dispatch))(StorageNew);
