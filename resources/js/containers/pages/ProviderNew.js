import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProviderForm from '~/containers/forms/Provider';
import {
  addProvider,
} from '~/actions/providers';

class ProviderNew extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    const {
      history,
      addProvider,
    } = this.props;

    addProvider(values).then((response) => {
      history.replace(`/providers/${response.payload.data.data.id}`);
    });
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Add provider</Typography>
        <ProviderForm
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
}), dispatch => bindActionCreators({
  addProvider,
}, dispatch))(ProviderNew);
