import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProviderProductForm from '~/containers/forms/ProviderProduct';
import {
  getProviders,
  getProviderProduct,
  updateProviderProduct,
} from '~/actions/providers';
import { getProducts } from '~/actions/products';

class ProviderProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: null,
      providerProductPivot: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match: {
        params: { provider_id, product_id },
      },
      getProviders,
      getProviderProduct,
      getProducts,
    } = this.props;

    getProviders();
    getProducts();
    // getProviderProduct(provider_id, product_id);
  }

  static getDerivedStateFromProps(nextProps) {
    const {
      match: {
        params: { provider_id, product_id },
      },
      providers,
      products: rawProducts,
    } = nextProps;

    const products = rawProducts.map(product => ({
      ...product,
      name: product.model,
    }));

    const provider = providers
      .find(provider => provider.id === parseInt(provider_id, 10));

    const providerProductPivot = provider && product_id && provider.products
      .find(product => product.id === parseInt(product_id, 10)).pivot;

    return {
      products,
      providerProductPivot,
    };
  }

  handleSubmit(values) {
    const {
      match: {
        params: { provider_id, product_id },
      },
      history,
      updateProviderProduct,
    } = this.props;

    updateProviderProduct(provider_id, product_id, values).then(() => {
      history.push(`/providers/${provider_id}/products`);
    });
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;
    const {
      products,
      providerProductPivot,
    } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Edit provider product</Typography>
        <ProviderProductForm
          initialValues={providerProductPivot}
          products={products}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  providers: state.providers,
  products: state.products,
}), dispatch => bindActionCreators({
  getProviders,
  getProviderProduct,
  updateProviderProduct,
  getProducts,
}, dispatch))(ProviderProduct);
