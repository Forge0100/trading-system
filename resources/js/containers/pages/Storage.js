import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import StorageForm from '~/containers/forms/Storage';
import {
  getStorage,
  updateStorage,
} from '~/actions/storages';

class Storage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      storage: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match,
      getStorage,
    } = this.props;

    getStorage(match.params.id);
  }

  static getDerivedStateFromProps(nextProps) {
    const { storages, match } = nextProps;

    return {
      storage: storages.find(storage => storage.id === parseInt(match.params.id, 10)),
    };
  }

  handleSubmit(values) {
    const {
      match,
      updateStorage,
    } = this.props;

    updateStorage(match.params.id, values);
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;
    const { storage } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Edit storage</Typography>
        <StorageForm
          initialValues={storage}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  storages: state.storages,
}), dispatch => bindActionCreators({
  getStorage,
  updateStorage,
}, dispatch))(Storage);
