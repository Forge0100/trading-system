import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import UserForm from '~/containers/forms/User';
import {
  getUser,
  addUser,
  updateUser,
} from '~/actions/users';

class User extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match,
      getUser,
    } = this.props;

    getUser(match.params.id);
  }

  static getDerivedStateFromProps(nextProps) {
    const { users, match } = nextProps;

    return {
      user: users.find(user => user.id === parseInt(match.params.id, 10)),
    };
  }

  handleSubmit(values) {
    const {
      match,
      updateUser,
    } = this.props;

    updateUser(match.params.id, values);
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;
    const { user } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Edit user</Typography>
        <UserForm
          initialValues={user}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  users: state.users,
}), dispatch => bindActionCreators({
  getUser,
  updateUser,
}, dispatch))(User);
