import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProviderForm from '~/containers/forms/Provider';
import {
  getProvider,
  updateProvider,
} from '~/actions/providers';

class Provider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      provider: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match,
      getProvider,
    } = this.props;

    getProvider(match.params.id);
  }

  static getDerivedStateFromProps(nextProps) {
    const { providers, match } = nextProps;

    return {
      provider: providers.find(provider => provider.id === parseInt(match.params.id, 10)),
    };
  }

  handleSubmit(values) {
    const {
      match,
      updateProvider,
    } = this.props;

    updateProvider(match.params.id, values);
  }

  render() {
    const { handleSubmit } = this;
    const { token } = this.props;
    const { provider } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Edit provider</Typography>
        <ProviderForm
          initialValues={provider}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  providers: state.providers,
}), dispatch => bindActionCreators({
  getProvider,
  updateProvider,
}, dispatch))(Provider);
