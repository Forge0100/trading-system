import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Table from '~/components/Table';
import { getProviders, deleteProviders } from '~/actions/providers';

class Providers extends Component {
  constructor(props) {
    super(props);

    this.handleAddButtonClick = this.handleAddButtonClick.bind(this);
    this.handleRowsDeleteClick = this.handleRowsDeleteClick.bind(this);
  }

  componentDidMount() {
    const { getProviders } = this.props;

    getProviders();
  }

  handleAddButtonClick() {
    const { history } = this.props;

    history.push('/providers/new');
  }

  handleRowsDeleteClick(ids) {
    const { deleteProviders } = this.props;

    deleteProviders(ids);
  }

  render() {
    const {
      handleAddButtonClick,
      handleEditButtonClick,
      handleRowsDeleteClick,
    } = this;
    const { token, providers } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Table
        title="Providers"
        columns={[
          {
            name: 'name',
            label: 'Name',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'phone',
            label: 'Phone',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'email',
            label: 'Email',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'address',
            label: 'Address',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'id',
            label: 'Actions',
            options: {
              filter: false,
              sort: false,
              empty: true,
              customBodyRender: value => (
                <Fragment>
                  <Button
                    component={Link}
                    color="primary"
                    to={`/providers/${value}/products`}
                  >
                    Edit products
                  </Button>
                  <Button
                    component={Link}
                    color="primary"
                    to={`/providers/${value}`}
                  >
                    Edit
                  </Button>
                </Fragment>
              ),
            },
          },
        ]}
        data={providers}
        options={{
          onAddButtonClick: handleAddButtonClick,
          onRowsDeleteClick: handleRowsDeleteClick,
        }}
      />
    );
  }
}

export default connect(state => ({
  token: state.token,
  providers: state.providers,
}), dispatch => bindActionCreators({
  getProviders,
  deleteProviders,
}, dispatch))(Providers);
