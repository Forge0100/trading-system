import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Paper from '~/components/Paper';
import ProductForm from '~/containers/forms/Product';
import {
  getProduct,
  updateProduct,
} from '~/actions/products';
import { getProductTypes } from '~/actions/productTypes';

class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const {
      match,
      getProduct,
      getProductTypes,
    } = this.props;

    getProduct(match.params.id);
    getProductTypes();
  }

  static getDerivedStateFromProps(nextProps) {
    const { products, match } = nextProps;

    return {
      product: products.find(product => product.id === parseInt(match.params.id, 10)),
    };
  }

  handleSubmit(values) {
    const {
      match,
      updateProduct,
    } = this.props;

    updateProduct(match.params.id, values);
  }

  render() {
    const { handleSubmit } = this;
    const {
      token,
      productTypes,
    } = this.props;
    const { product } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Paper>
        <Typography variant="h6" gutterBottom>Edit product</Typography>
        <ProductForm
          initialValues={product}
          productTypeOptions={productTypes}
          onSubmit={handleSubmit}
        />
      </Paper>
    );
  }
}

export default connect(state => ({
  token: state.token,
  products: state.products,
  productTypes: state.productTypes,
}), dispatch => bindActionCreators({
  getProduct,
  updateProduct,
  getProductTypes,
}, dispatch))(Product);
