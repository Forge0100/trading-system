import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Table from '~/components/Table';
import {
  getProviders,
  getProviderProducts,
  deleteProviderProducts,
} from '~/actions/providers';

class ProviderProducts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: null,
    };

    this.handleAddButtonClick = this.handleAddButtonClick.bind(this);
    this.handleEditButtonClick = this.handleEditButtonClick.bind(this);
    this.handleRowsDeleteClick = this.handleRowsDeleteClick.bind(this);
  }

  componentDidMount() {
    const {
      match: {
        params: { provider_id },
      },
      getProviders,
      getProviderProducts,
    } = this.props;

    getProviders();
    // getProviderProducts(provider_id);
  }

  static getDerivedStateFromProps(nextProps) {
    const {
      match: {
        params: { provider_id },
      },
      providers,
    } = nextProps;

    const provider = providers.find(provider => provider.id === parseInt(provider_id, 10));
    const products = provider && provider.products && provider.products.map(product => ({
      id: product.id,
      name: product.model,
      price: product.pivot.price,
    }));

    return {
      products,
    };
  }

  handleAddButtonClick() {
    const {
      match: {
        params: { provider_id },
      },
      history,
    } = this.props;

    history.push(`/providers/${provider_id}/products/new`);
  }

  handleEditButtonClick(product_id) {
    const {
      match: {
        params: { provider_id },
      },
      history,
    } = this.props;

    history.push(`/providers/${provider_id}/products/${product_id}`);
  }

  handleRowsDeleteClick(product_ids) {
    const {
      match: {
        params: { provider_id },
      },
      deleteProviderProducts,
    } = this.props;

    deleteProviderProducts(provider_id, product_ids);
  }

  render() {
    const {
      handleAddButtonClick,
      handleEditButtonClick,
      handleRowsDeleteClick,
    } = this;
    const { token } = this.props;
    const { products } = this.state;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Table
        title="Provider Products"
        columns={[
          {
            name: 'name',
            label: 'Name',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'price',
            label: 'Price',
            options: {
              filter: true,
              sort: true,
            },
          },
        ]}
        data={products}
        options={{
          onAddButtonClick: handleAddButtonClick,
          onEditButtonClick: handleEditButtonClick,
          onRowsDeleteClick: handleRowsDeleteClick,
        }}
      />
    );
  }
}

export default connect(state => ({
  token: state.token,
  providers: state.providers,
  products: state.products,
}), dispatch => bindActionCreators({
  getProviders,
  getProviderProducts,
  deleteProviderProducts,
}, dispatch))(ProviderProducts);
