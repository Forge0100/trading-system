import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Table from '~/components/Table';
import { getProducts, deleteProducts } from '~/actions/products';

class Products extends Component {
  constructor(props) {
    super(props);

    this.handleAddButtonClick = this.handleAddButtonClick.bind(this);
    this.handleEditButtonClick = this.handleEditButtonClick.bind(this);
    this.handleRowsDeleteClick = this.handleRowsDeleteClick.bind(this);
  }

  componentDidMount() {
    const { getProducts } = this.props;

    getProducts();
  }

  handleAddButtonClick() {
    const { history } = this.props;

    history.push('/products/new');
  }

  handleEditButtonClick(index) {
    const { history } = this.props;

    history.push(`/products/${index}`);
  }

  handleRowsDeleteClick(ids) {
    const { deleteProducts } = this.props;

    deleteProducts(ids);
  }

  render() {
    const {
      handleAddButtonClick,
      handleEditButtonClick,
      handleRowsDeleteClick,
    } = this;
    const { token, products } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <Table
        title="Products"
        columns={[
          {
            name: 'model',
            label: 'Model',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'producer',
            label: 'Producer',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'price',
            label: 'Price',
            options: {
              filter: true,
              sort: true,
            },
          },
          {
            name: 'warranty',
            label: 'Warranty',
            options: {
              filter: true,
              sort: true,
              customBodyRender: value => `${value}%`,
            },
          },
        ]}
        data={products}
        options={{
          onAddButtonClick: handleAddButtonClick,
          onEditButtonClick: handleEditButtonClick,
          onRowsDeleteClick: handleRowsDeleteClick,
        }}
      />
    );
  }
}

export default connect(state => ({
  token: state.token,
  products: state.products,
}), dispatch => bindActionCreators({
  getProducts,
  deleteProducts,
}, dispatch))(Products);
