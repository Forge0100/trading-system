import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Index extends Component {
  render() {
    const { token } = this.props;

    if (!token) {
      return <Redirect to="/login" />;
    }

    return (
      <p>Main page</p>
    );
  }
}

export default connect(state => ({
  token: state.token,
}))(Index);
