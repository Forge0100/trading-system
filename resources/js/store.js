import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import axiosMiddleware from 'redux-axios-middleware';
import { api, axiosMiddlewareOptions } from '~/api';

export default createStore(rootReducer, composeWithDevTools(applyMiddleware(
  thunk,
  axiosMiddleware(api, axiosMiddlewareOptions),
)));
