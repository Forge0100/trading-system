export default (state = [], action) => {
  switch (action.type) {
    case 'GET_USERS_SUCCESS':
      return action.payload.data.data;

    case 'GET_USER_SUCCESS':
    case 'POST_USER_SUCCESS':
    case 'UPDATE_USER_SUCCESS':
      return [
        ...state.filter(user => user.id !== action.payload.data.data.id),
        action.payload.data.data,
      ];

    default:
      return state;
  }
};
