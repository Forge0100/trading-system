import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import token from './token';
import products from './products';
import productTypes from './productTypes';
import users from './users';
import providers from './providers';
import storages from './storages';

export default combineReducers({
  token,
  products,
  productTypes,
  users,
  providers,
  storages,
  form: formReducer,
});
