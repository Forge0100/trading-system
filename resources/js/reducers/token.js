export default (state = localStorage.getItem('token'), action) => {
  switch (action.type) {
    case 'SET_AUTH_SUCCESS':
    case 'GET_AUTH_SUCCESS': {
      const { token } = action.payload.data.data;

      localStorage.setItem('token', token);
      return token;
    }

    case 'RESET_AUTH':
      localStorage.removeItem('token');
      return null;

    default:
      return state;
  }
};
