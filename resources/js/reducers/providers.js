export default (state = [], action) => {
  switch (action.type) {
    case 'GET_PROVIDERS_SUCCESS':
      return action.payload.data.data;

    case 'GET_PROVIDER_SUCCESS':
    case 'POST_PROVIDER_SUCCESS':
    case 'UPDATE_PROVIDER_SUCCESS':
      return [
        ...state.filter(provider => provider.id !== action.payload.data.data.id),
        action.payload.data.data,
      ];

    /*case 'GET_PROVIDER_PRODUCTS_SUCCESS': {
      const { provider_id } = action.meta.previousAction.payload;

      return [
        ...state.filter(provider => provider.id !== parseInt(provider_id, 10)),
        {
          ...state.find(provider => provider.id === parseInt(provider_id, 10)),
          id: parseInt(provider_id, 10),
          products: action.payload.data.data,
        },
      ];
    }*/

    /*case 'GET_PROVIDER_PRODUCT_SUCCESS':
    case 'UPDATE_PROVIDER_PRODUCT_SUCCESS': {
      const { provider_id } = action.meta.previousAction.payload;
      const provider = state
        .find(provider => provider.id === parseInt(provider_id, 10)) || { products: [] };
      const product_id = action.payload.data.data.id;

      return [
        ...state.filter(provider => provider.id !== parseInt(provider_id, 10)),
        {
          ...provider,
          products: [
            ...provider.products.filter(product => product.id !== product_id),
            action.payload.data.data,
          ],
        },
      ];
    }*/

    default:
      return state;
  }
};
