export default (state = [], action) => {
  switch (action.type) {
    case 'GET_STORAGES_SUCCESS':
      return action.payload.data.data;

    case 'GET_STORAGE_SUCCESS':
    case 'POST_STORAGE_SUCCESS':
    case 'UPDATE_STORAGE_SUCCESS':
      return [
        ...state.filter(storage => storage.id !== action.payload.data.data.id),
        action.payload.data.data,
      ];

    default:
      return state;
  }
};
