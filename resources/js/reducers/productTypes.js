const initState = [
  {
    id: 0,
    name: 'Default',
  },
];

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_PRODUCT_TYPES_SUCCESS':
      return action.payload.data.data;

    default:
      return state;
  }
};
