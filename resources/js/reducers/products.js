export default (state = [], action) => {
  switch (action.type) {
    case 'GET_PRODUCTS_SUCCESS':
      return action.payload.data.data;

    case 'GET_PRODUCT_SUCCESS':
    case 'POST_PRODUCT_SUCCESS':
    case 'UPDATE_PRODUCT_SUCCESS':
      return [
        ...state.filter(product => product.id !== action.payload.data.data.id),
        action.payload.data.data,
      ];

    default:
      return state;
  }
};
