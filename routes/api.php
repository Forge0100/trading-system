<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
  Route::post('register', 'AuthController@register');
  Route::post('login', 'AuthController@login');

  Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/user', function (Request $request) {
      return $request->user();
    });

    Route::apiResource('products', 'ProductController');
    Route::apiResource('productTypes', 'ProductTypeController');
    Route::apiResource('users', 'UserController');
    Route::apiResource('providers', 'ProviderController');
    Route::apiResource('providers.products', 'ProviderProductController');
    Route::apiResource('storages', 'StorageController');
    Route::apiResource('storages.products', 'StorageProductController');
    Route::apiResource('orders', 'OrderController');
    Route::apiResource('orders.details', 'OrderDetailController');
  });
});
