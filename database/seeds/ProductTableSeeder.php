<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
      factory(App\Product::class, 100)->create()->each(function ($product) use ($faker) {
        factory(App\Provider::class, 5)->create()->each(function ($provider) use ($faker, $product) {
          if (!$faker->boolean()) {
            return;
          }

          $provider->products()->attach($product->id, ["price" => $faker->randomFloat(2, 0.1, 99999)]);
        });

        factory(App\Storage::class, 2)->create()->each(function ($storage) use ($faker, $product) {
          if (!$faker->boolean()) {
            return;
          }

          $storage->products()->attach($product->id);
        });
      });
    }
}
