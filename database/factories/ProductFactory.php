<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
      'model' => $faker->word,
      'producer' => $faker->word,
      'warranty' => $faker->randomElement([1, 2, 6, 9, 12, 24, 36]),
      'price' => $faker->randomFloat(2, 0.1, 99999),
      'description' => $faker->text,
      'product_type_id' => function () {
        return factory(App\ProductType::class)->create()->id;
      },
    ];
});
