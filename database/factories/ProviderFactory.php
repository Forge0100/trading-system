<?php

use App\Provider;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    return [
      'name' => $faker->word,
      'phone' => $faker->phoneNumber,
      'email' => $faker->unique()->safeEmail,
      'address' => $faker->address,
    ];
});
