<?php

use App\Passport;
use Faker\Generator as Faker;

$factory->define(Passport::class, function (Faker $faker) {
  return [
    'series' => $faker->randomLetter . $faker->randomLetter,
    'number' => $faker->idNumber,
    'issued_by' => $faker->dateTimeThisDecade(),
    'user_id' => function () {
      return factory(App\User::class)->create()->id;
    },
  ];
});
