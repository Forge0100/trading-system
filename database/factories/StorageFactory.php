<?php

use App\Storage;
use Faker\Generator as Faker;

$factory->define(Storage::class, function (Faker $faker) {
  return [
    'name' => $faker->word,
    'address' => $faker->address,
  ];
});
