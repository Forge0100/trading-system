<?php

use App\OrderDetail;
use Faker\Generator as Faker;

$factory->define(OrderDetail::class, function (Faker $faker) {
  return [
    'product_quantity' => $faker->numberBetween(1, 99),
  ];
});
