<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('model');
            $table->string('producer')->nullable();
            $table->decimal('price');
            $table->integer('warranty')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('product_type_id');
            $table->timestamps();

            $table->foreign('product_type_id')->references('id')->on('product_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('products', function ($table) {
        $table->dropForeign(['product_type_id']);
      });

        Schema::dropIfExists('products');
    }
}
