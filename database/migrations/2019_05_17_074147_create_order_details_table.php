<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedBigInteger('product_id');
          $table->unsignedBigInteger('storage_id');
          $table->unsignedBigInteger('order_id');
          $table->integer('product_quantity');
          $table->timestamps();

          $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
          $table->foreign('storage_id')->references('id')->on('storages')->onDelete('cascade');
          $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('order_details', function ($table) {
        $table->dropForeign(['product_id']);
        $table->dropForeign(['storage_id']);
        $table->dropForeign(['order_id']);
      });

      Schema::dropIfExists('order_details');
    }
}
