<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductProviderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_provider', function (Blueprint $table) {
          $table->unsignedBigInteger('product_id');
          $table->unsignedBigInteger('provider_id');
          $table->decimal('price');
          $table->timestamps();

          $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
          $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('product_provider', function ($table) {
        $table->dropForeign(['provider_id']);
        $table->dropForeign(['product_id']);
      });

      Schema::dropIfExists('product_provider');
    }
}
