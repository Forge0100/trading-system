<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_storage', function (Blueprint $table) {
          $table->unsignedBigInteger('product_id');
          $table->unsignedBigInteger('storage_id');
          $table->timestamps();

          $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
          $table->foreign('storage_id')->references('id')->on('storages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('product_storage', function ($table) {
        $table->dropForeign(['product_id']);
        $table->dropForeign(['storage_id']);
      });

      Schema::dropIfExists('product_storage');
    }
}
