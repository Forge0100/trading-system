<?php

namespace App\Http\Controllers\API;

use App\Product;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class ProviderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  $provider_id
     * @return \Illuminate\Http\Response
     */
    public function index($provider_id)
    {
      $provider = Provider::findOrFail($provider_id);
      $products = $provider->products;

      return $this->sendResponse($products->toArray(), 'Products retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $provider_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $provider_id)
    {
      $input = $request->all();

      $provider = Provider::findOrFail($provider_id);

      $provider->products()->attach($input['product_id'], ['price' => $input['price']]);

      return $this->sendResponse(null, 'Product attached successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $provider_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function show($provider_id, $product_id)
    {
      $provider = Provider::findOrFail($provider_id);

      $product = $provider->products()->findOrFail($product_id);

      return $this->sendResponse($product->toArray(), 'Product retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $provider_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $provider_id, $product_id)
    {
      $input = $request->all();

      $provider = Provider::findOrFail($provider_id);

      $provider->products()->updateExistingPivot($product_id, ['price' => $input['price']]);

      return $this->sendResponse(null, 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $provider_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($provider_id, $product_id)
    {
      $product_ids = explode(",", $product_id);

      $provider = Provider::findOrFail($provider_id);

      $provider->products()->detach($product_ids);

      return $this->sendResponse(null, 'Product(s) detached successfully.');
    }
}
