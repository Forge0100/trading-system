<?php

namespace App\Http\Controllers\API;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $storages = Order::all();

      return $this->sendResponse($storages->toArray(), 'Orders retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'status' => 'required|numeric|between:0,4',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $order = new Order;
      $order->status = $input['status'];
      $order->save();

      return $this->sendResponse($order->toArray(), 'Order created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $order = Order::findOrFail($id);

      if (is_null($order)) {
        return $this->sendError('Order not found.');
      }

      return $this->sendResponse($order->toArray(), 'Order retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'status' => 'required|numeric|between:0,',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $order = Order::findOrFail($id);

      $order->status = $input['status'];
      $order->save();

      return $this->sendResponse($order->toArray(), 'Order updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(",", $id);

      Order::destroy($ids);

      return $this->sendResponse(null, 'Order(s) deleted successfully.');
    }
}
