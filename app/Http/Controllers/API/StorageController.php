<?php

namespace App\Http\Controllers\API;

use App\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $storages = Storage::all();

      return $this->sendResponse($storages->toArray(), 'Storages retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'name' => 'required',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $storage = new Storage;
      $storage->name = $input['name'];
      $storage->address = $input['address'];
      $storage->save();

      return $this->sendResponse($storage->toArray(), 'Storage created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $storage = Storage::findOrFail($id);

      if (is_null($storage)) {
        return $this->sendError('Storage not found.');
      }

      return $this->sendResponse($storage->toArray(), 'Storage retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'name' => 'required',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $storage = Storage::findOrFail($id);

      $storage->name = $input['name'];
      $storage->address = $input['address'];
      $storage->save();

      return $this->sendResponse($storage->toArray(), 'Storage updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(",", $id);

      Storage::destroy($ids);

      return $this->sendResponse(null, 'Storage(s) deleted successfully.');
    }
}
