<?php

namespace App\Http\Controllers\API;

use App\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as Controller;

class StorageProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  $storage_id
     * @return \Illuminate\Http\Response
     */
    public function index($storage_id)
    {
      $storage = Storage::findOrFail($storage_id);
      $products = $storage->products;

      return $this->sendResponse($products->toArray(), 'Products retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $storage_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $storage_id)
    {
      $input = $request->all();

      $storage = Storage::findOrFail($storage_id);

      $storage->products()->attach($input['product_id'], ['price' => $input['price']]);

      return $this->sendResponse(null, 'Product attached successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $storage_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function show($storage_id, $product_id)
    {
      $storage = Storage::findOrFail($storage_id);

      $product = $storage->products()->findOrFail($product_id);

      return $this->sendResponse($product->toArray(), 'Product retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $storage_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $storage_id, $product_id)
    {
      $input = $request->all();

      $storage = Storage::findOrFail($storage_id);

      $storage->products()->updateExistingPivot($product_id, ['price' => $input['price']]);

      return $this->sendResponse(null, 'Product updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $storage_id
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($storage_id, $product_id)
    {
      $product_ids = explode(",", $product_id);

      $storage = Storage::findOrFail($storage_id);

      $storage->products()->detach($product_ids);

      return $this->sendResponse(null, 'Product(s) detached successfully.');
    }
}
