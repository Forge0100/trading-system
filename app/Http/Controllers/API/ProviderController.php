<?php

namespace App\Http\Controllers\API;

use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class ProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $providers = Provider::with('products.type')->get();

      return $this->sendResponse($providers->toArray(), 'Providers retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'name' => 'required',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $product = new Provider;
      $product->name = $input['name'];
      $product->phone = $input['phone'];
      $product->email = $input['email'];
      $product->address = $input['address'];
      $product->save();

      return $this->sendResponse($product->toArray(), 'Provider created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $product = Provider::findOrFail($id);

      if (is_null($product)) {
        return $this->sendError('Provider not found.');
      }

      return $this->sendResponse($product->toArray(), 'Provider retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'name' => 'required',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $product = Provider::findOrFail($id);

      $product->name = $input['name'];
      $product->phone = $input['phone'];
      $product->email = $input['email'];
      $product->address = $input['address'];
      $product->save();

      return $this->sendResponse($product->toArray(), 'Provider updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(",", $id);

      Provider::destroy($ids);

      return $this->sendResponse(null, 'Provider(s) deleted successfully.');
    }
}
