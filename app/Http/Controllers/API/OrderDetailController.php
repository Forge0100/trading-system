<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($order_id)
    {
      $order = Order::findOrFail($order_id);
      $details = $order->details;

      return $this->sendResponse($details->toArray(), 'Order details retrieved successfully.');
    }

    /**
     * Store a newly created resource in detail.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $order_id)
    {
      $input = $request->all();

      $order = Order::findOrFail($order_id);

      $validator = Validator::make($input, [
        'product_quantity' => 'required|numeric',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $product = Product::findOrFail($input['product_id']);
      $storage = Product::findOrFail($input['storage_id']);

      $detail = $order->details();
      $detail->product_id = $product->id;
      $detail->storage_id = $storage->id;
      $detail->order_id = $order->id;
      $detail->product_quantity = $input['product_quantity'];
      $detail->save();

      return $this->sendResponse($detail->toArray(), 'Order detail created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($order_id, $order_detail_id)
    {
      $order = Order::findOrFail($order_id);

      $detail = $order->details()->findOrFail($order_detail_id);

      return $this->sendResponse($detail->toArray(), 'Order detail retrieved successfully.');
    }

    /**
     * Update the specified resource in detail.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id, $order_detail_id)
    {
      $input = $request->all();

      $validator = Validator::make($input, [
        'product_quantity' => 'required|numeric',
      ]);

      if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
      }

      $order = OrderDetail::findOrFail($order_id);
      $detail = $order->details()->findOrFail($order_detail_id);

      $product = Product::findOrFail($input['product_id']);
      $storage = Product::findOrFail($input['storage_id']);

      $detail->product_id = $product->id;
      $detail->storage_id = $storage->id;
      $detail->order_id = $order->id;
      $detail->product_quantity = $input['product_quantity'];
      $detail->save();

      return $this->sendResponse($order->toArray(), 'Order detail updated successfully.');
    }

    /**
     * Remove the specified resource from detail.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id, $order_detail_id)
    {
      $order_detail_ids = explode(",", $order_detail_id);

      $order = OrderDetail::findOrFail($order_id);
      $order->details()->destroy($order_detail_ids);

      return $this->sendResponse(null, 'Order detail(s) deleted successfully.');
    }
}
