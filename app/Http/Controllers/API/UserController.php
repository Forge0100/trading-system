<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\BaseController as Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();

      return $this->sendResponse($users->toArray(), 'Users retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $user = new User;
      $user->name = $input['name'];
      $user->surname = $input['surname'];
      $user->patronymic = $input['patronymic'];
      $user->date_of_birth = $input['date_of_birth'];
      $user->phone = $input['phone'];
      $user->email = $input['email'];
      $user->save();

      return $this->sendResponse($user->toArray(), 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::findOrFail($id);

      if (is_null($user)) {
        return $this->sendError('User not found.');
      }

      return $this->sendResponse($user->toArray(), 'User retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();

      $user = User::findOrFail($id);

      $user->name = $input['name'];
      $user->surname = $input['surname'];
      $user->patronymic = $input['patronymic'];
      $user->date_of_birth = $input['date_of_birth'];
      $user->phone = $input['phone'];
      $user->email = $input['email'];
      $user->save();

      return $this->sendResponse($user->toArray(), 'User updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $ids = explode(",", $id);

      User::destroy($ids);

      return $this->sendResponse(null, 'User(s) deleted successfully.');
    }
}
