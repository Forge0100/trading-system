<?php

namespace App\Http\Controllers\API;

use App\ProductType;
use App\Http\Controllers\API\BaseController as Controller;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $productTypes = ProductType::all();

      return $this->sendResponse($productTypes->toArray(), 'Product types retrieved successfully.');
    }
}
