<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  public function type()
  {
    return $this->belongsTo(ProductType::class, 'product_type_id');
  }

  public function providers()
  {
    return $this->belongsToMany('App\Provider');
  }

  public function storages()
  {
    return $this->belongsToMany('App\Storage');
  }
}
