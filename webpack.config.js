const path = require('path');
const webpack = require('webpack');
const dotenv = require('dotenv');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

const env = dotenv.config().parsed;
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'eslint-loader',
            options: {
              emitWarning: true,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.json', '.vue'],
    alias: {
      '~': path.resolve(__dirname, './resources/js'),
    },
  },
  plugins: [
    new webpack.DefinePlugin(envKeys),
    new SWPrecacheWebpackPlugin({
      cacheId: 'pwa',
      filename: 'service-worker.js',
      staticFileGlobs: ['public/**/*.{css,eot,svg,ttf,woff,woff2,js,html}'],
      minify: true,
      stripPrefix: 'public/',
      handleFetch: true,
      dynamicUrlToDependencies: {
        '/': ['resources/views/app.blade.php'],
      },
      staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
      runtimeCaching: [
        {
          urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
          handler: 'cacheFirst',
        },
        {
          urlPattern: /^https:\/\/www\.thecocktaildb\.com\/images\/media\/drink\/(\w+)\.jpg/,
          handler: 'cacheFirst',
        },
      ],
    }),
  ],
};
